package zisis.mobile.novibetdemo.mvp.interactor

import com.google.gson.Gson
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.CompletableDeferred
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import zisis.mobile.novibetdemo.models.parsers.headLines.HeadLineParser
import zisis.mobile.novibetdemo.models.parsers.headLines.HeadLineResponse
import zisis.mobile.novibetdemo.models.viewModels.headLine.toModelList
import zisis.mobile.novibetdemo.mvp.interactor.home.HomeInteractorImpl
import zisis.mobile.novibetdemo.providers.network.NetworkProvider

class HomeInteractorImplTest {

    private val gson = Gson()
    private val headLineResponse = gson.fromJson<HeadLineParser>(
        HomeInteractorImplTest::class.java.getResource("/HeaderLineResponse.js")!!.readText(),
        HeadLineParser::class.java
    )

    private val competitor1Caption = "Ελλάδα"
    private val competitor2Caption = "Νέα Ζηλανδία"

    @Mock
    lateinit var networkProvider: NetworkProvider

    private lateinit var homeInteractorImpl: HomeInteractorImpl

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        homeInteractorImpl = HomeInteractorImpl(networkProvider)
    }

    @After
    fun tearDown() {
        homeInteractorImpl.detach()
    }

    @ExperimentalCoroutinesApi
    @Test
    fun getHeaderLine() = runBlocking {
        whenever(networkProvider.getHeadLinesAsync()).thenReturn(
            CompletableDeferred(
                headLineResponse.headerList
            )
        )
        val response = homeInteractorImpl.getHeaderLine()
        Assert.assertNotNull(response)
        Assert.assertNotNull(response.data?.get(0))
        Assert.assertEquals(competitor1Caption, response.data?.get(0)?.competitor1Caption)
        Assert.assertEquals(competitor2Caption, response.data?.get(0)?.competitor2Caption)
    }
}