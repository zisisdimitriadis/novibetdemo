package zisis.mobile.novibetdemo.mvp.presenter.base

import com.nhaarman.mockitokotlin2.whenever
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import zisis.mobile.novibetdemo.mvp.interactor.base.BaseInteractor
import zisis.mobile.novibetdemo.mvp.interactor.base.MVPInteractor
import zisis.mobile.novibetdemo.mvp.view.base.MVPView

class BasePresenterTest {

    @Mock
    lateinit var view: MVPView

    lateinit var interactor: BaseInteractor

    lateinit var presenter: BasePresenter<MVPView, MVPInteractor>

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        interactor = BaseInteractor()
        presenter = BasePresenter(view, interactor)
    }

    @Test
    fun detach() {
        whenever(view.isAttached()).thenReturn(true)

        Assert.assertNotNull(presenter.getView())
        Assert.assertNotNull(presenter.getInteractor())
        assert(presenter.isViewAttached())

        presenter.detach()
        interactor.detach()

        Assert.assertNull(presenter.getView())
        Assert.assertNull(presenter.getInteractor())
        assert(!presenter.isViewAttached())
    }

    @Test
    fun getView() {
        Assert.assertNotNull(presenter.getView())
    }

    @Test
    fun getInteractor() {
        Assert.assertNotNull(presenter.getInteractor())
    }

    @Test
    fun isViewAttached() {
        whenever(view.isAttached()).thenReturn(true)
        assert(presenter.isViewAttached())
        whenever(view.isAttached()).thenReturn(false)
        assert(!presenter.isViewAttached())
        whenever(view.isAttached()).thenReturn(true)
        presenter.detach()
        assert(!presenter.isViewAttached())
    }
}