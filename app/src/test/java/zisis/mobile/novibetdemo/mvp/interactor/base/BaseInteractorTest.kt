package zisis.mobile.novibetdemo.mvp.interactor.base

import com.nhaarman.mockitokotlin2.verify
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations

class BaseInteractorTest {

    @Mock
    lateinit var baseIntarctor: BaseInteractor

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
    }

    @Test
    fun detach() {
        baseIntarctor.detach()
        verify(baseIntarctor).detach()
    }

}