package zisis.mobile.novibetdemo.mvp.presenter.home

import com.google.gson.Gson
import com.nhaarman.mockitokotlin2.atLeastOnce
import com.nhaarman.mockitokotlin2.validateMockitoUsage
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.cancel
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.TestCoroutineScope
import kotlinx.coroutines.test.runBlockingTest
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import zisis.mobile.novibetdemo.models.common.DataResult
import zisis.mobile.novibetdemo.models.parsers.headLines.HeadLineParser
import zisis.mobile.novibetdemo.models.viewModels.headLine.HeadLineModel
import zisis.mobile.novibetdemo.models.viewModels.headLine.toModelList
import zisis.mobile.novibetdemo.mvp.interactor.HomeInteractorImplTest
import zisis.mobile.novibetdemo.mvp.interactor.home.HomeInteractor
import zisis.mobile.novibetdemo.mvp.view.home.HomeView

class HomePresenterTest {

    private val gson = Gson()
    private val headLineResponse = gson.fromJson<HeadLineParser>(
        HomePresenterTest::class.java.getResource("/HeaderLineResponse.js")!!.readText(),
        HeadLineParser::class.java
    )


    @ExperimentalCoroutinesApi
    private val testUiDispatcher = TestCoroutineDispatcher()
    @ExperimentalCoroutinesApi
    private val testBgDispatcher = TestCoroutineDispatcher()
    @ExperimentalCoroutinesApi
    private val testScope = TestCoroutineScope()

    @Mock
    lateinit var view: HomeView
    @Mock
    lateinit var interactor: HomeInteractor
    lateinit var presenter: HomePresenterImpl

    @ExperimentalCoroutinesApi
    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        presenter = HomePresenterImpl(view, interactor)
        presenter.updateDispatchersForTests(testUiDispatcher, testBgDispatcher, testScope)
    }

    @OptIn(ExperimentalCoroutinesApi::class)
    @Test
    fun testInit() = testScope.run {
        whenever(view.isAttached()).thenReturn(true)
        presenter.init()
    }

    @Test
    fun getWeatherForecast() = runBlockingTest {
        whenever(view.isAttached()).thenReturn(true)
        whenever(interactor.getHeaderLine()).thenReturn(
            DataResult(headLineResponse.headerList.toModelList())
        )

        presenter.init()
        verify(view, atLeastOnce()).isAttached()
        verify(view).showHomeView(headLineResponse.headerList.toModelList() as ArrayList<HeadLineModel>, arrayListOf())
    }


    @ExperimentalCoroutinesApi
    @After
    fun tearDown() {
        presenter.detach()
        verify(interactor).detach()
        testUiDispatcher.cancel()
        testBgDispatcher.cancel()
        validateMockitoUsage()
    }

}