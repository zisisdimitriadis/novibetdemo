{
"test": [
  {
    "betViews": [
      {
        "betViewKey": "308870",
        "modelType": "TwoCompetitors",
        "betContextId": 12517702,
        "marketViewGroupId": 1560778,
        "marketViewId": 1663261,
        "rootMarketViewGroupId": 440818,
        "path": "basketball/greece-vs-new-zealand",
        "startTime": "15:00",
        "competitor1Caption": "Ελλάδα",
        "competitor2Caption": "Νέα Ζηλανδία",
        "marketTags": [],
        "betItems": [
          {
            "id": 903332978,
            "code": "1",
            "caption": "1 (-14,5)",
            "instanceCaption": "-14,5",
            "price": 2,
            "oddsText": "2.00",
            "isAvailable": true
          },
          {
            "id": 903332979,
            "code": "2",
            "caption": "2 (+14,5)",
            "instanceCaption": "+14,5",
            "price": 1.75,
            "oddsText": "1.75",
            "isAvailable": true
          }
        ],
        "liveData": {
          "remaining": "00:06:34.9718008",
          "remainingSeconds": 394.9718008,
          "homePoints": 82,
          "awayPoints": 70,
          "quarterScores": [
            {
              "caption": "1η Π",
              "homeScore": 28,
              "awayScore": 19
            },
            {
              "caption": "2η Π",
              "homeScore": 23,
              "awayScore": 25
            },
            {
              "caption": "Ημ",
              "homeScore": 51,
              "awayScore": 44
            },
            {
              "caption": "3η Π",
              "homeScore": 20,
              "awayScore": 22
            },
            {
              "caption": "4η Π",
              "homeScore": 11,
              "awayScore": 4
            }
          ],
          "homePossession": false,
          "supportsAchievements": true,
          "liveStreamingCountries": "GR",
          "sportradarMatchId": 17686566,
          "referenceTime": "2019-09-05T13:37:22Z",
          "referenceTimeUnix": 1567690642,
          "elapsed": "00:03:25.0281992",
          "elapsedSeconds": 205.0281992,
          "duration": "00:10:00",
          "durationSeconds": 600,
          "timeToNextPhase": null,
          "timeToNextPhaseSeconds": null,
          "phaseSysname": "BASKETBALL_GAME_FOURTH_QUARTER",
          "phaseCaption": "4η Π",
          "phaseCaptionLong": "4η Περίοδος",
          "isLive": true,
          "isInPlay": true,
          "isInPlayPaused": false,
          "isInterrupted": false,
          "supportsActions": true,
          "timeline": null,
          "adjustTimeMillis": 5
        },
        "displayFormat": "LiveBasketball",
        "text": "",
        "url": null,
        "imageId": 49927
      }
    ],
    "caption": "Home Headlines (GR)",
    "marketViewType": "HEADLINES",
    "marketViewKey": "M182398",
    "modelType": "default"
  }
]
}