package zisis.mobile.novibetdemo.ui.activity.splash

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AppCompatActivity
import zisis.mobile.novibetdemo.R
import zisis.mobile.novibetdemo.common.application.NovibetApplication
import zisis.mobile.novibetdemo.mvp.interactor.splash.SplashInteractorImpl
import zisis.mobile.novibetdemo.mvp.presenter.splash.SplashPresenter
import zisis.mobile.novibetdemo.mvp.presenter.splash.SplashPresenterImpl
import zisis.mobile.novibetdemo.mvp.view.splash.SplashView
import zisis.mobile.novibetdemo.ui.activity.BaseMvpActivity
import zisis.mobile.novibetdemo.ui.activity.home.HomeActivity

class SplashActivity : BaseMvpActivity<SplashPresenter>(), SplashView {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        presenter = SplashPresenterImpl(
            this, SplashInteractorImpl(
                NovibetApplication.get().networkProvider,
                NovibetApplication.get().sharedPreferencesProvider
            )
        )
        presenter?.init()
    }

    override fun goToHomeScreen() {
        startActivity(Intent(this, HomeActivity::class.java))
    }

    override fun showLoginFail() {
        showDialog(titleText = getString(R.string.dialog_error_title),
            messageText = getString(R.string.login_fail),
            rightButtonListener = {
                it.dismiss()
                finish()
            })
    }
}