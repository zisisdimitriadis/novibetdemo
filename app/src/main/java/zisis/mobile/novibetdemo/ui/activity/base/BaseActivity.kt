package zisis.mobile.novibetdemo.ui.activity.base

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.afollestad.materialdialogs.DialogCallback
import com.afollestad.materialdialogs.MaterialDialog
import kotlinx.android.synthetic.main.activity_root.*
import zisis.mobile.novibetdemo.R

open class BaseActivity : AppCompatActivity() {

    private var materialDialog: MaterialDialog? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun setContentView(layoutResID: Int) {
        super.setContentView(R.layout.activity_root)
        val layout = layoutInflater.inflate(layoutResID, containerView, false)
        containerView?.addView(layout)
    }

    override fun onResume() {
        super.onResume()
    }

    override fun onPause() {
        super.onPause()
    }

    override fun onDestroy() {
        super.onDestroy()
        materialDialog?.dismiss()
    }


    fun showDialog(
        titleText: String = "",
        messageText: String = "",
        leftButtonText: String = "",
        rightButtonText: String = "",
        leftButtonListener: DialogCallback? = null,
        rightButtonListener: DialogCallback? = null
    ) {

        materialDialog?.dismiss()
        materialDialog = MaterialDialog(this).show {
            title(text = titleText)
            message(text = messageText)
            positiveButton(text = rightButtonText) {
                rightButtonListener?.invoke(this)
            }
            negativeButton(text = leftButtonText) {
                leftButtonListener?.invoke(this)
            }

        }
    }

    fun showErrorDialog(
        error: String = "",
        errorDescription: String = "",
        buttonText: String = getString(android.R.string.ok)
    ) {
        showDialog(
            titleText = error,
            messageText = errorDescription,
            rightButtonText = buttonText,
            rightButtonListener = {}
        )
    }

}