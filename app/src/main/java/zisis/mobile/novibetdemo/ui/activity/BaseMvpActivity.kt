package zisis.mobile.novibetdemo.ui.activity

import android.view.View
import kotlinx.android.synthetic.main.layout_empty.*
import kotlinx.android.synthetic.main.layout_loading.*
import zisis.mobile.novibetdemo.R
import zisis.mobile.novibetdemo.mvp.interactor.base.MVPInteractor
import zisis.mobile.novibetdemo.mvp.presenter.base.MVPPresenter
import zisis.mobile.novibetdemo.mvp.view.base.MVPView
import zisis.mobile.novibetdemo.ui.activity.base.BaseActivity

open class BaseMvpActivity<T : MVPPresenter<MVPView, MVPInteractor>> : BaseActivity(), MVPView {

    protected var presenter: T? = null

    override fun isAttached(): Boolean {
        return !isFinishing
    }

    override fun showLoader() {
        hideEmpty()
        loadingView?.visibility = View.VISIBLE
    }

    override fun hideLoader() {
        loadingView?.visibility = View.GONE
    }

    override fun showEmpty() {
        hideLoader()
        emptyTextView?.text = getString(R.string.empty_data_text)
        emptyView?.visibility = View.VISIBLE
    }

    override fun hideEmpty() {
        emptyView?.visibility = View.GONE
    }

    override fun showGenericError() {
        showErrorDialog(
            getString(R.string.dialog_error_title),
            getString(R.string.generic_error),
            getString(R.string.dialog_error_button)
        )
    }

    override fun showNoInternetError() {
        showErrorDialog(
            getString(R.string.dialog_error_title),
            getString(R.string.no_internet),
            getString(R.string.dialog_error_button)
        )
    }

    override fun showError(error: String) {
        showErrorDialog(
            getString(R.string.dialog_error_title),
            error,
            getString(R.string.dialog_error_button)
        )
    }
}