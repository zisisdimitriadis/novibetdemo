package zisis.mobile.novibetdemo.ui.adapters.headerLines

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.row_headline_item.*
import zisis.mobile.novibetdemo.R
import zisis.mobile.novibetdemo.models.viewModels.headLine.HeadLineModel


class HeaderLineRecyclerViewAdapter() :
    RecyclerView.Adapter<HeaderLineRecyclerViewAdapter.HeaderLineViewHolder>() {

    private var headLineModelList: ArrayList<HeadLineModel> = arrayListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HeaderLineViewHolder {
        return HeaderLineViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.row_headline_item, parent, false)
        )
    }

    override fun onBindViewHolder(holder: HeaderLineViewHolder, position: Int) {
       holder.bind(headLineModelList[position])
    }

    override fun getItemCount(): Int {
        return headLineModelList.size
    }

    fun setHeadLines(headLineModelList: ArrayList<HeadLineModel>) {
        this.headLineModelList = headLineModelList
        notifyDataSetChanged()
    }

    class HeaderLineViewHolder(override val containerView: View) :
        RecyclerView.ViewHolder(containerView),
        LayoutContainer {

        fun bind(model: HeadLineModel) {
            competitorOneTextView.text = model.competitor1Caption
            competitorTwoTextView.text = model.competitor2Caption
            startTimeTextView.text = model.startTime

            val layoutManager = LinearLayoutManager(
                itemView.context,
                LinearLayoutManager.HORIZONTAL,
                false
            )
        }
    }
}