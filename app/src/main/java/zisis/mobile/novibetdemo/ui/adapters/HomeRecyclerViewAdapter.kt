package zisis.mobile.novibetdemo.ui.adapters

import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.NonNull
import androidx.core.os.HandlerCompat.postDelayed
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.PagerSnapHelper
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.row_game_item.*
import kotlinx.android.synthetic.main.row_header_line.*
import zisis.mobile.novibetdemo.R
import zisis.mobile.novibetdemo.common.extensions.toTimeElapsed
import zisis.mobile.novibetdemo.models.viewModels.game.GameModel
import zisis.mobile.novibetdemo.models.viewModels.headLine.HeadLineModel
import zisis.mobile.novibetdemo.ui.adapters.headerLines.HeaderLineRecyclerViewAdapter
import zisis.mobile.novibetdemo.ui.recyclerview.itemDecoration.StartEndDividerItemDecoration

class HomeRecyclerViewAdapter() : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var gameModelList: ArrayList<GameModel> = arrayListOf()
    private var headLineModelList: ArrayList<HeadLineModel> = arrayListOf()
    private var startEndDividerItemDecoration: StartEndDividerItemDecoration? = null

    private var showPositionToHeader: Int = 0

    companion object {
        private const val TYPE_HEADER_LINE = 0
        private const val TYPE_GAME = 1
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (viewType == TYPE_HEADER_LINE) {
            val startEndMargin = 0
            val betweenMargin =
                parent.context.resources?.getDimensionPixelSize(R.dimen.common_margin_ten) ?: 0
            startEndDividerItemDecoration = StartEndDividerItemDecoration(
                parent.context,
                DividerItemDecoration.HORIZONTAL,
                startEndMargin,
                startEndMargin,
                betweenMargin
            )

            return HeaderLineViewHolder(
                LayoutInflater.from(parent.context).inflate(R.layout.row_header_line, parent, false)
            )
        } else {
            return GameViewHolder(
                LayoutInflater.from(parent.context).inflate(R.layout.row_game_item, parent, false)
            )

        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is HeaderLineViewHolder -> {
                Handler().postDelayed({
                    if (showPositionToHeader >= headLineModelList.size) {
                        showPositionToHeader = 0
                    } else {
                        showPositionToHeader++
                    }
                    holder.headersRecyclerView.smoothScrollToPosition(showPositionToHeader)
                }, 5000L)
                holder.bind(headLineModelList, startEndDividerItemDecoration)
            }
            is GameViewHolder -> {
                holder.bind(gameModelList[position - 1])
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        val headerListSize = headLineModelList.size
        val gamesListSize = gameModelList.size

        if (headerListSize > 0 && gamesListSize > 0) {
            return if (position == 0) {
                TYPE_HEADER_LINE
            } else {
                TYPE_GAME
            }
        }
        if (headerListSize == 0 && gamesListSize > 0) {
            return TYPE_GAME
        }
        if (headerListSize > 0 && gamesListSize == 0) {
            return TYPE_HEADER_LINE
        }
        return super.getItemViewType(position)
    }

    override fun getItemCount(): Int {
        if (headLineModelList.isEmpty()) {
            return gameModelList.size
        }
        return if (gameModelList.isEmpty()) {
            1
        } else {
            gameModelList.size + 1
        }
    }

    fun setHomeModelList(headerList: ArrayList<HeadLineModel>, gamesList: ArrayList<GameModel>) {
        this.headLineModelList = headerList
        this.gameModelList = gamesList
        notifyDataSetChanged()
    }

    class HeaderLineViewHolder(override val containerView: View) :
        RecyclerView.ViewHolder(containerView), LayoutContainer {

        fun bind(
            headerList: ArrayList<HeadLineModel>,
            startEndDividerItemDecoration: StartEndDividerItemDecoration?
        ) {
            val headerLineRecyclerViewAdapter = HeaderLineRecyclerViewAdapter()
            val layoutManager = LinearLayoutManager(
                itemView.context,
                LinearLayoutManager.HORIZONTAL,
                false
            )
            headersRecyclerView?.layoutManager = layoutManager
            headersRecyclerView?.adapter = headerLineRecyclerViewAdapter
            headersRecyclerView.onFlingListener = null
            val snapHelper = PagerSnapHelper()
            snapHelper.attachToRecyclerView(headersRecyclerView)
            startEndDividerItemDecoration?.let {
                headersRecyclerView?.removeItemDecoration(it)
                headersRecyclerView?.addItemDecoration(it)
            }
            headerLineRecyclerViewAdapter.setHeadLines(headerList)


        }
    }

    class GameViewHolder(override val containerView: View) :
        RecyclerView.ViewHolder(containerView), LayoutContainer {

        fun bind(gameModel: GameModel) {
            competitorOneTextView.text = gameModel.competitor1
            competitorTwoTextView.text = gameModel.competitor2
            ellapsedTimeTextView.text = gameModel.elapsed.toTimeElapsed()
        }
    }
}
