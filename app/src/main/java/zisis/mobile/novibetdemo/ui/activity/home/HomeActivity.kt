package zisis.mobile.novibetdemo.ui.activity.home

import android.content.Intent
import android.os.Bundle
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_home.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import zisis.mobile.novibetdemo.R
import zisis.mobile.novibetdemo.common.application.NovibetApplication
import zisis.mobile.novibetdemo.models.eventBus.GameEvent
import zisis.mobile.novibetdemo.models.eventBus.HeadLineEvent
import zisis.mobile.novibetdemo.models.viewModels.game.GameModel
import zisis.mobile.novibetdemo.models.viewModels.headLine.HeadLineModel
import zisis.mobile.novibetdemo.mvp.interactor.home.HomeInteractorImpl
import zisis.mobile.novibetdemo.mvp.presenter.home.HomePresenter
import zisis.mobile.novibetdemo.mvp.presenter.home.HomePresenterImpl
import zisis.mobile.novibetdemo.mvp.view.home.HomeView
import zisis.mobile.novibetdemo.providers.sharedPreferences.SharedPreferencesProvider
import zisis.mobile.novibetdemo.service.PollingService
import zisis.mobile.novibetdemo.ui.activity.BaseMvpActivity
import zisis.mobile.novibetdemo.ui.adapters.HomeRecyclerViewAdapter
import zisis.mobile.novibetdemo.ui.recyclerview.itemDecoration.BottomTopDividerItemDecoration
import zisis.mobile.novibetdemo.ui.recyclerview.itemDecoration.RecyclerViewMarginDecoration
import java.util.*


class HomeActivity : BaseMvpActivity<HomePresenter>(), HomeView {

    private var homeRecyclerViewAdapter: HomeRecyclerViewAdapter? = null
    private lateinit var layoutManager: LinearLayoutManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        presenter =
            HomePresenterImpl(this, HomeInteractorImpl(NovibetApplication.get().networkProvider))

        initLayout()
        presenter?.init()

        EventBus.getDefault().getStickyEvent(HeadLineEvent::class.java)?.let { headLineEvent ->
            handleHeadLineEvent(headLineEvent)
        }

        EventBus.getDefault().getStickyEvent(GameEvent::class.java)?.let { gameEvent ->
            handleGameEvent(gameEvent)
        }
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    fun onGameEvent(gameEvent: GameEvent) {
        presenter?.updateGameList(gameEvent.gamesList)
    }

    private fun handleGameEvent(gameEvent: GameEvent) {
       presenter?.updateGameList(gameEvent.gamesList)
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    fun onHeaderLine(headLineEvent: HeadLineEvent) {
        handleHeadLineEvent(headLineEvent)
    }

    private fun handleHeadLineEvent(headLineEvent: HeadLineEvent) {
      presenter?.updateHeadLineList(headLineEvent.headLineModelList)
    }

    override fun onResume() {
        super.onResume()
        EventBus.getDefault().register(this)
    }

    override fun onPause() {
        super.onPause()
        EventBus.getDefault().unregister(this)
    }

    override fun onDestroy() {
        super.onDestroy()
        if (EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().unregister(this)
    }

    override fun onBackPressed() {
        finishAffinity();
    }

    private fun initLayout() {
        layoutManager = LinearLayoutManager(this)
        homeRecyclerView?.layoutManager = layoutManager
        homeRecyclerViewAdapter = HomeRecyclerViewAdapter()
        homeRecyclerView?.adapter = homeRecyclerViewAdapter
        val decoration = RecyclerViewMarginDecoration(
            resources.getDimension(R.dimen.common_margin_ten).toInt(),
            1
        )
        val topBottomMargin =
            resources?.getDimensionPixelSize(R.dimen.common_margin_ten) ?: 0
        val betweenMargin =
            resources?.getDimensionPixelSize(R.dimen.common_margin_ten) ?: 0
        homeRecyclerView?.addItemDecoration(
            BottomTopDividerItemDecoration(
                this,
                DividerItemDecoration.VERTICAL,
                topBottomMargin,
                topBottomMargin,
                betweenMargin
            )
        )
        homeRecyclerView?.addItemDecoration(decoration)

    }

    override fun showHomeView(
        headerList: ArrayList<HeadLineModel>,
        gamesList: ArrayList<GameModel>
    ) {
        homeRecyclerViewAdapter?.setHomeModelList(headerList, gamesList)
        if (!NovibetApplication.get().isMyServiceRunning(PollingService::class.java)) {
            NovibetApplication.get().initLifeCycleListener()
        }
    }
}

