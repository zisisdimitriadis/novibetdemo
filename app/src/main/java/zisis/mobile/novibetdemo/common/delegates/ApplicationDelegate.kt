package zisis.mobile.novibetdemo.common.delegates

import android.app.Application
import timber.log.Timber
import zisis.mobile.novibetdemo.BuildConfig
import zisis.mobile.novibetdemo.common.delegates.base.BaseDelegate

class ApplicationDelegate(
    application: Application
) : BaseDelegate<Application>(application) {

    fun initTimber() {
        if (BuildConfig.DEBUG)
            Timber.plant(Timber.DebugTree())
    }

}