package zisis.mobile.novibetdemo.common.application

import android.annotation.SuppressLint
import android.app.ActivityManager
import android.app.Application
import android.content.ComponentName
import android.content.ServiceConnection
import android.os.IBinder
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import androidx.lifecycle.ProcessLifecycleOwner
import timber.log.Timber
import zisis.mobile.novibetdemo.common.delegates.ApplicationDelegate
import zisis.mobile.novibetdemo.providers.network.NetworkProvider
import zisis.mobile.novibetdemo.providers.network.api.NovibetClient
import zisis.mobile.novibetdemo.providers.sharedPreferences.SharedPreferencesProvider
import zisis.mobile.novibetdemo.service.PollingService

class NovibetApplication : Application() {

    companion object {
        private lateinit var instance: NovibetApplication

        @JvmStatic
        fun get(): NovibetApplication {
            return instance
        }
    }

    private val applicationDelegate = ApplicationDelegate(this)

    val networkProvider: NetworkProvider by lazy {
        return@lazy NovibetClient(this)
    }

    val sharedPreferencesProvider: SharedPreferencesProvider by lazy {
        return@lazy SharedPreferencesProvider(this)
    }

    var isInForeground: Boolean = false
        private set
        @Synchronized get

    private var pollingService: PollingService? = null
    private val pollingServiceConnection: ServiceConnection = object : ServiceConnection {
        override fun onServiceDisconnected(name: ComponentName?) {
            pollingService = null
        }

        @SuppressLint("SetTextI18n")
        override fun onServiceConnected(name: ComponentName?, binder: IBinder?) {
            val pollingServiceBinder = binder as PollingService.PollingServiceBinder?
            pollingService = pollingServiceBinder?.service
            pollingService?.startPeriodicUpdates(networkProvider)
        }
    }

    override fun onCreate() {
        super.onCreate()
        instance = this
        applicationDelegate.initTimber()
    }

    fun initLifeCycleListener() {
        ProcessLifecycleOwner.get().lifecycle.addObserver(object : LifecycleObserver {
            @OnLifecycleEvent(Lifecycle.Event.ON_START)
            fun onMoveToForeground() {
                Timber.d("NovibetApplication > LifecycleObserver > onMoveToForeground")
                isInForeground = true
                startPeriodicUpdates()
            }

            @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
            fun onMoveToBackground() {
                Timber.d("NovibetApplication > LifecycleObserver > onMoveToBackground")
                isInForeground = false
                stopPeriodicUpdates()
            }
        })
    }

    fun startPeriodicUpdates() {
        PollingService.startService(this)
        PollingService.connectToService(this, pollingServiceConnection)
    }

    fun stopPeriodicUpdates() {
        pollingService?.stopPeriodicUpdates()
        PollingService.disconnectToService(this, pollingServiceConnection)
        PollingService.stopService(this)
    }

    fun isMyServiceRunning(serviceClass: Class<*>): Boolean {
        val manager = getSystemService(ACTIVITY_SERVICE) as ActivityManager
        for (service in manager.getRunningServices(Int.MAX_VALUE)) {
            if (serviceClass.name == service.service.className) {
                Timber.d("NovibetApplication > Polling Service is Running")
                return true

            }
        }
        Timber.d("NovibetApplication > Polling Service is NOT Running")
        return false
    }

}