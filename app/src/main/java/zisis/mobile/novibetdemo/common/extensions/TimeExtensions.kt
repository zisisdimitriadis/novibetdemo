package zisis.mobile.novibetdemo.common.extensions

import zisis.mobile.novibetdemo.common.extensions.TimeValues.INVALID_TIME


object TimeValues {
    const val INVALID_TIME: Long = -1
}

fun String.toSeconds(): Long {
    val subStrings = this.split(":")
    if (subStrings.isNullOrEmpty() || subStrings.size != 3) {
        return INVALID_TIME
    }
    if (subStrings[0].startsWith("-")) {
        return INVALID_TIME
    }

    return subStrings[0].toLong() * 3600 + subStrings[1].toLong() * 60 + subStrings[2].toDouble()
        .toLong()

}

fun String.toTimeElapsed(): String {
    var hoursString = ""
    var minutesString = ""
    var secondsString = ""
    val elapsedTimeInSeconds = this.toSeconds()
    if (elapsedTimeInSeconds == INVALID_TIME) {
        return "ΕΛΗΞΕ"
    }
    val hours = elapsedTimeInSeconds / 3600
    val minutes = (elapsedTimeInSeconds % 3600) / 60
    val seconds = ((elapsedTimeInSeconds % 3600) % 60)

    println("Test hours $hours")
    println("Test minutes $minutes")
    println("Test seconds $seconds")

    hoursString = when (hours) {
        in 1..9 -> {
            "0".plus(hours.toString()) + ":"
        }
        0L -> {
            ""
        }
        else -> {
            "$hours:"
        }
    }

    minutesString = if (minutes in 1..9) {
        "0".plus(minutes.toString()) + ":"
    } else {
        "$minutes:"
    }

    secondsString = if (seconds in 1..9) {
        "0".plus(seconds.toString())
    } else {
        seconds.toString()
    }

    return hoursString.plus(minutesString).plus(secondsString)

}