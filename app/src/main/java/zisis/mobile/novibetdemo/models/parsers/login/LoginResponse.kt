package zisis.mobile.novibetdemo.models.parsers.login

import com.google.gson.annotations.SerializedName

class LoginResponse(
    @SerializedName("access_token") private val accessToken: String? = null,
    @SerializedName("token_type") private val tokenType: String? = null
) {

    fun isSuccess(): Boolean {
        return !accessToken.isNullOrEmpty() && tokenType == "Bearer"
    }

    fun getAccessToken(): String? {
        return accessToken
    }

}