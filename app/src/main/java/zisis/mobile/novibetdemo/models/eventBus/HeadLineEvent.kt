package zisis.mobile.novibetdemo.models.eventBus

import zisis.mobile.novibetdemo.models.viewModels.headLine.HeadLineModel

data class HeadLineEvent(val headLineModelList: List<HeadLineModel>) {
}