package zisis.mobile.novibetdemo.models.common

import zisis.mobile.novibetdemo.models.common.Model.Companion.INVALID_INT
import zisis.mobile.novibetdemo.models.common.Model.Companion.INVALID_STRING

interface Model {

    companion object {
        const val INVALID_STRING = ""
        const val INVALID_INT = -1
    }
}

fun Int?.nonNull(defaultValue: Int = INVALID_INT): Int {
    if (this == null) {
        return defaultValue
    }
    return this
}

fun String?.nonNull(defaultValue: String = INVALID_STRING): String {
    if (this == null) {
        return defaultValue
    }
    return this
}