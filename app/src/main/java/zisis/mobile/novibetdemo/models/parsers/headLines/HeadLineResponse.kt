package zisis.mobile.novibetdemo.models.parsers.headLines

import com.google.gson.annotations.SerializedName
import java.util.*

data class HeadLineResponse(
    @SerializedName("betViews") val betViewsList: List<BetView>? = null,
    @SerializedName("caption") val caption: String? = null,
    @SerializedName("marketViewType") val marketViewType: String? = null,
    @SerializedName("marketViewKey") val marketViewKey: String? = null,
    @SerializedName("modelType") val modelType: String? = null
) {
}

data class BetView(
    @SerializedName("betViewKey") val betViewKey: String? = null,
    @SerializedName("modelType") val modelType: String? = null,
    @SerializedName("betContextId") val betContextId: Int? = null,
    @SerializedName("marketViewGroupId") val marketViewGroupId: Int? = null,
    @SerializedName("rootMarketViewGroupId") val rootMarketViewGroupId: Int? = null,
    @SerializedName("path") val path: String? = null,
    @SerializedName("startTime") val startTime: String? = null,
    @SerializedName("competitor1Caption") val competitor1Caption: String? = null,
    @SerializedName("competitor2Caption") val competitor2Caption: String? = null,
    @SerializedName("marketTags") val marketTagsList: List<Objects>? = null,
    @SerializedName("betItems") val betItemsList: List<BetItem>? = null,
    @SerializedName("liveData") val liveData: LiveData? = null,
    @SerializedName("displayFormat") val displayFormat: String? = null,
    @SerializedName("text") val text: String? = null,
    @SerializedName("url") val url: String? = null,
    @SerializedName("imageId") val imageId: Int? = null,
) {}

data class BetItem(
    @SerializedName("id") val id: Int? = null,
    @SerializedName("code") val code: String? = null,
    @SerializedName("caption") val caption: String? = null,
    @SerializedName("instanceCaption") val instanceCaption: String? = null,
    @SerializedName("price") val price: Double? = null,
    @SerializedName("oddsText") val oddsText: String? = null,
    @SerializedName("isAvailable") val isAvailable: Boolean? = null,
) {}

data class LiveData(
    @SerializedName("remaining") val remaining: String? = null,
    @SerializedName("remainingSeconds") val remainingSeconds: Double? = null,
    @SerializedName("homePoints") val homePoints: Int? = null,
    @SerializedName("awayPoints") val awayPoints: Int? = null,
    @SerializedName("quarterScores") val quarterScores: List<QuarterScore>? = null,
    @SerializedName("homePossession") val homePossession: Boolean? = null,
    @SerializedName("supportsAchievements") val supportsAchievements: Boolean? = null,
    @SerializedName("liveStreamingCountries") val liveStreamingCountries: String? = null,
    @SerializedName("sportradarMatchId") val sportradarMatchId: Int? = null,
    @SerializedName("referenceTime") val referenceTime: String? = null,
    @SerializedName("referenceTimeUnix") val referenceTimeUnix: Int? = null,
    @SerializedName("elapsed") val elapsed: String? = null,
    @SerializedName("elapsedSeconds") val elapsedSeconds: Double? = null,
    @SerializedName("duration") val duration: String? = null,
    @SerializedName("durationSeconds") val durationSeconds: Int? = null,
    @SerializedName("timeToNextPhase") val timeToNextPhase: Objects? = null,
    @SerializedName("timeToNextPhaseSeconds") val timeToNextPhaseSeconds: Objects? = null,
    @SerializedName("phaseSysname") val phaseSysname: String? = null,
    @SerializedName("phaseCaption") val phaseCaption: String? = null,
    @SerializedName("phaseCaptionLong") val phaseCaptionLong: String? = null,
    @SerializedName("isLive") val isLive: Boolean? = null,
    @SerializedName("isInPlay") val isInPlay: Boolean? = null,
    @SerializedName("isInPlayPaused") val isInPlayPaused: Boolean? = null,
    @SerializedName("isInterrupted") val isInterrupted: Boolean? = null,
    @SerializedName("supportsActions") val supportsActions: Boolean? = null,
    @SerializedName("timeline") val timeline: Objects? = null,
    @SerializedName("adjustTimeMillis") val adjustTimeMillis: Int? = null,
) {}

data class QuarterScore(
    @SerializedName("caption") val caption: String? = null,
    @SerializedName("homeScore") val homeScore: Int? = null,
    @SerializedName("awayScore") val awayScore: Int? = null
) {}