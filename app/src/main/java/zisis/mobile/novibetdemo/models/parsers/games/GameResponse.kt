package zisis.mobile.novibetdemo.models.parsers.games

import com.google.gson.annotations.SerializedName
import zisis.mobile.novibetdemo.models.parsers.headLines.BetItem
import java.util.*

data class GameResponse(
    @SerializedName("betViews") val betViews: List<GameBetView>? = null,
    @SerializedName("hasHighlights") val hasHighlights: Boolean? = null,
    @SerializedName("totalCount") val totalCount: Int? = null,
    @SerializedName("caption") val caption: String? = null,
    @SerializedName("marketViewType") val marketViewType: String? = null,
    @SerializedName("modelType") val modelType: String? = null,

) {
}

data class GameBetView(
    @SerializedName("competitionContextCaption") val competitionContextCaption: String? = null,
    @SerializedName("competitions") val competitions: List<Competition>? = null,
    @SerializedName("totalCount") val totalCount: Int? = null,
    @SerializedName("marketCaptions") val marketCaptions: List<MarketCaption>? = null,
    @SerializedName("betViewKey") val betViewKey: String? = null,
    @SerializedName("modelType") val modelType: String? = null

) {}

data class Competition(
    @SerializedName("betContextId") val betContextId: Int? = null,
    @SerializedName("caption") val caption: String? = null,
    @SerializedName("regionCaption") val regionCaption: String? = null,
    @SerializedName("events") val events: List<Event>? = null,

) {}

data class Event(
    @SerializedName("betContextId") val betContextId: Int? = null,
    @SerializedName("path") val path: String? = null,
    @SerializedName("isHighlighted") val isHighlighted: Boolean? = null,
    @SerializedName("additionalCaptions") val additionalCaptions: AdditionalCaptions? = null,
    @SerializedName("liveData") val liveData: GameLiveData? = null,
    @SerializedName("hasBetContextInfo") val hasBetContextInfo: Boolean? = null,
    @SerializedName("markets") val markets: List<Market>? = null
) {}

data class GameLiveData(
    @SerializedName("homeGoals") val homeGoals: Int? = null,
    @SerializedName("awayGoals") val awayGoals: Int? = null,
    @SerializedName("homeCorners") val homeCorners: Int? = null,
    @SerializedName("awayCorners") val awayCorners: Int? = null,
    @SerializedName("homeYellowCards") val homeYellowCards: Int? = null,
    @SerializedName("awayYellowCards") val awayYellowCards: Int? = null,
    @SerializedName("homeRedCards") val homeRedCards: Int? = null,
    @SerializedName("awayRedCards") val awayRedCards: Int? = null,
    @SerializedName("homePenaltyKicks") val homePenaltyKicks: Int? = null,
    @SerializedName("awayPenaltyKicks") val awayPenaltyKicks: Int? = null,
    @SerializedName("supportsAchievements") val supportsAchievements: Boolean? = null,
    @SerializedName("liveStreamingCountries") val liveStreamingCountries: String? = null,
    @SerializedName("sportradarMatchId") val sportradarMatchId: Int? = null,
    @SerializedName("referenceTime") val referenceTime: String? = null,
    @SerializedName("referenceTimeUnix") val referenceTimeUnix: Int? = null,
    @SerializedName("elapsed") val elapsed: String? = null,
    @SerializedName("elapsedSeconds") val elapsedSeconds: Double? = null,
    @SerializedName("duration") val duration: String? = null,
    @SerializedName("durationSeconds") val durationSeconds: Double? = null,
    @SerializedName("timeToNextPhase") val timeToNextPhase: String? = null,
    @SerializedName("timeToNextPhaseSeconds") val timeToNextPhaseSeconds: Double? = null,
    @SerializedName("phaseSysname") val phaseSysname: String? = null,
    @SerializedName("phaseCaption") val phaseCaption: String? = null,
    @SerializedName("phaseCaptionLong") val phaseCaptionLong: String? = null,
    @SerializedName("isLive") val isLive: Boolean? = null,
    @SerializedName("isInPlay") val isInPlay: Boolean? = null,
    @SerializedName("isInPlayPaused") val isInPlayPaused: Boolean? = null,
    @SerializedName("isInterrupted") val isInterrupted: Boolean? = null,
    @SerializedName("supportsActions") val supportsActions: Boolean? = null,
    @SerializedName("timeline") val timeline: Objects? = null,
    @SerializedName("adjustTimeMillis") val adjustTimeMillis: Int? = null
) {}

data class Market(
    @SerializedName("marketId") val marketId: Int? = null,
    @SerializedName("marbetTypeSysnameketId") val betTypeSysname: String? = null,
    @SerializedName("betItems") val betItems: List<BetItem>? = null,

    ) {}

data class MarketCaption(
    @SerializedName("betTypeSysname") val betTypeSysname: String? = null,
    @SerializedName("marketCaption") val marketCaption: String? = null,
    @SerializedName("betCaptions") val betCaptions: Objects? = null,
) {}

data class AdditionalCaptions(
    @SerializedName("type") val type: Int? = null,
    @SerializedName("competitor1") val competitor1: String? = null,
    @SerializedName("competitor1ImageId") val competitor1ImageId: Int? = null,
    @SerializedName("competitor2") val competitor2: String? = null,
    @SerializedName("competitor2ImageId") val competitor2ImageId: Int? = null
)