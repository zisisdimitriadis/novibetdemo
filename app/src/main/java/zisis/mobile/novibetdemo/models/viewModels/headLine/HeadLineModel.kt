package zisis.mobile.novibetdemo.models.viewModels.headLine

import zisis.mobile.novibetdemo.models.common.Model
import zisis.mobile.novibetdemo.models.common.Model.Companion.INVALID_STRING
import zisis.mobile.novibetdemo.models.common.nonNull
import zisis.mobile.novibetdemo.models.parsers.headLines.HeadLineResponse

data class HeadLineModel(
    val competitor1Caption: String? = INVALID_STRING,
    val competitor2Caption: String? = INVALID_STRING,
    val startTime: String? = INVALID_STRING
) : Model {

}

fun HeadLineResponse.toModelList(): List<HeadLineModel> {
    val headLineModelList = arrayListOf<HeadLineModel>()
    this.betViewsList?.filter { betView ->
        !betView.competitor1Caption.isNullOrBlank() && !betView.competitor2Caption.isNullOrBlank()
    }?.map { betView ->
        headLineModelList.add(
            HeadLineModel(
                betView.competitor1Caption.nonNull(),
                betView.competitor2Caption.nonNull(),
                betView.startTime.nonNull()
            )
        )
    }
    return headLineModelList
}

fun List<HeadLineResponse>.toModelList(): List<HeadLineModel> {
    return this.flatMap { headLineResponse ->
        headLineResponse.toModelList()
    }
}