package zisis.mobile.novibetdemo.models.eventBus

import zisis.mobile.novibetdemo.models.viewModels.game.GameModel

data class GameEvent(val gamesList: List<GameModel>) {
}