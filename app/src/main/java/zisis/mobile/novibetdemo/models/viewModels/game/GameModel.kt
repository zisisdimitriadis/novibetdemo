package zisis.mobile.novibetdemo.models.viewModels.game

import zisis.mobile.novibetdemo.models.common.Model
import zisis.mobile.novibetdemo.models.common.Model.Companion.INVALID_STRING
import zisis.mobile.novibetdemo.models.common.nonNull
import zisis.mobile.novibetdemo.models.parsers.games.GameResponse

data class GameModel(
    val competitor1: String = INVALID_STRING,
    val competitor2: String = INVALID_STRING,
    val elapsed: String = INVALID_STRING
) : Model {

}

fun GameResponse?.toModelList(): List<GameModel> {
    var gameModelList = arrayListOf<GameModel>()

    gameModelList = this?.betViews?.flatMap { gameBetView ->
        gameBetView.competitions ?: arrayListOf()
    }?.flatMap { competition ->
        competition.events ?: arrayListOf()
    }?.map { event ->
        GameModel(
            competitor1 = event.additionalCaptions?.competitor1.nonNull(),
            competitor2 = event.additionalCaptions?.competitor2.nonNull(),
            elapsed = event.liveData?.elapsed.nonNull()
        )
    } as ArrayList<GameModel>
    return gameModelList
}

fun List<GameResponse>.toModelList(): List<GameModel> {
    return this.flatMap { gameResponse ->
        gameResponse.toModelList()
    }
}

fun List<GameModel>.getModel(homeTeam: String, awayTeam: String): GameModel? {
    return this.firstOrNull {
        it.competitor1 == homeTeam && it.competitor2 == awayTeam
    }
}