package zisis.mobile.novibetdemo.models.common

data class DataResult<out T>(
    val data: T? = null,
    val throwable: Throwable? = null
)