package zisis.mobile.novibetdemo.service

import android.app.Service
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.Binder
import android.os.IBinder
import kotlinx.coroutines.*
import org.greenrobot.eventbus.EventBus
import timber.log.Timber
import zisis.mobile.novibetdemo.models.eventBus.GameEvent
import zisis.mobile.novibetdemo.models.eventBus.HeadLineEvent
import zisis.mobile.novibetdemo.models.viewModels.game.toModelList
import zisis.mobile.novibetdemo.models.viewModels.headLine.toModelList
import zisis.mobile.novibetdemo.providers.network.NetworkProvider

class PollingService: Service() {

    companion object {
        @JvmStatic
        fun startService(context: Context) {
            try {
                val serviceIntent = Intent(context, PollingService::class.java)
                context.startService(serviceIntent)
            } catch (exception: Exception) {
                Timber.d("PollingService unable to start Service with error: %s", exception.printStackTrace().toString())
            }
        }

        @JvmStatic
        fun stopService(context: Context) {
            val serviceIntent = Intent(context, PollingService::class.java)
            context.stopService(serviceIntent)
        }

        @JvmStatic
        fun connectToService(context: Context, serviceConnection: ServiceConnection) {
            val serviceIntent = Intent(context, PollingService::class.java)
            context.bindService(serviceIntent, serviceConnection, Context.BIND_AUTO_CREATE)
        }

        @JvmStatic
        fun disconnectToService(context: Context, serviceConnection: ServiceConnection) {
            context.unbindService(serviceConnection)
        }
    }

    private var job = SupervisorJob()
    private val bgScope = CoroutineScope(Dispatchers.IO + job)
    private val binder = PollingServiceBinder()

    private var countHead: Int = 0
    private var countGame: Int = 0

    private var isRunning: Boolean = false
        @Synchronized get() {
            return field
        }
        @Synchronized set(value) {
            field = value
        }

    inner class PollingServiceBinder : Binder() {
        val service: PollingService
            get() = this@PollingService
    }

    override fun onBind(intent: Intent?): IBinder? {
        return binder
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        return START_STICKY
    }

    override fun onDestroy() {
        super.onDestroy()
        stopRunning()
    }

    override fun onTaskRemoved(rootIntent: Intent?) {
        super.onTaskRemoved(rootIntent)
        stopRunning()
        stopSelf()
    }

    @Synchronized
    fun startPeriodicUpdates(networkProvider: NetworkProvider) {
        isRunning = true
        bgScope.launch {
            while (isRunning) {
                try {
                    fetchHeaderLines(networkProvider)
                    fetchGames(networkProvider)
                } catch (e: Exception) {
                    Timber.d(e)
                } finally {
                    delay(2 * 1000) //2 seconds delay
                }
            }
        }
    }

    @Synchronized
    fun stopPeriodicUpdates() {
        isRunning = false
    }

    private fun stopRunning() {
        stopPeriodicUpdates()
        bgScope.coroutineContext.cancelChildren()
    }

    private suspend fun fetchHeaderLines(networkProvider: NetworkProvider) {
        Timber.d("Polling Service -> fetchHeaderLines -> call api for: %s", ++countHead)
        val headerLineList = networkProvider.updatedHeadLinesAsync().await().toModelList()
        EventBus.getDefault().postSticky(HeadLineEvent(headerLineList))
    }

    private suspend fun fetchGames(networkProvider: NetworkProvider) {
        Timber.d("Polling Service -> fetchGames -> call api for: %s", ++countGame)
        val gamesList = networkProvider.updatedGamesAsync().await().toModelList()
        EventBus.getDefault().postSticky(GameEvent(gamesList))

    }
}