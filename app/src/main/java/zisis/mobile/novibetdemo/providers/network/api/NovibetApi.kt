package zisis.mobile.novibetdemo.providers.network.api

import kotlinx.coroutines.Deferred
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.GET
import retrofit2.http.POST
import zisis.mobile.novibetdemo.models.parsers.games.GameResponse
import zisis.mobile.novibetdemo.models.parsers.headLines.HeadLineResponse
import zisis.mobile.novibetdemo.models.parsers.login.LoginResponse

interface NovibetApi {

    @FormUrlEncoded
    @POST("5d8e4bd9310000a2612b5448/login")
    fun userLoginAsync(
        @Field("userName") username: String? = null,
        @Field("password") password: String? = null
    ): Deferred<LoginResponse>

    @GET("5d7113ef3300000e00779746")
    fun getHeadLinesAsync(): Deferred<List<HeadLineResponse>>

    @GET("5d7113513300000b2177973a")
    fun getGamesAsync(): Deferred<List<GameResponse>>

    @GET("5d7114b2330000112177974d")
    fun updateGamesAsync(): Deferred<List<GameResponse>>

    @GET("5d711461330000d135779748")
    fun updateHeadLinesAsync(): Deferred<List<HeadLineResponse>>
}