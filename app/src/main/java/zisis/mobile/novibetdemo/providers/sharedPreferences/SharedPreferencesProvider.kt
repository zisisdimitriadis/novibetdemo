package zisis.mobile.novibetdemo.providers.sharedPreferences

import android.content.Context
import android.content.SharedPreferences

class SharedPreferencesProvider(private val appContext: Context) {

    private val preferences: SharedPreferences =
        appContext.getSharedPreferences(PREFS_FILENAME, PRIVATE_MODE)

    companion object {
        private const val PRIVATE_MODE = 0
        private const val PREFS_FILENAME = "novibetDemoSharedPreferences"
        private const val IS_LOGGED_IN = "is_logged_in"
        private const val ACCESS_TOKEN = "access_token"
    }


    @Synchronized
    fun isUserLoggedIn(): Boolean {
        return preferences.getBoolean((IS_LOGGED_IN), false)
    }

    @Synchronized
    fun setUserLoggedIn(value: Boolean) {
        preferences.edit().putBoolean((IS_LOGGED_IN), value).apply()
    }

    @Synchronized
    fun getAccessToken(): String? {
        return preferences.getString((ACCESS_TOKEN), "")
    }

    @Synchronized
    fun setAccessToken(token: String) {
        preferences.edit().putString((ACCESS_TOKEN), token).apply()
    }

}