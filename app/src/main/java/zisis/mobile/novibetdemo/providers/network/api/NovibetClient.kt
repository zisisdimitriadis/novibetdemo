package zisis.mobile.novibetdemo.providers.network.api

import android.content.Context
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import kotlinx.coroutines.Deferred
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import zisis.mobile.novibetdemo.common.DefinitionsApi
import zisis.mobile.novibetdemo.models.parsers.games.GameResponse
import zisis.mobile.novibetdemo.models.parsers.headLines.HeadLineResponse
import zisis.mobile.novibetdemo.models.parsers.login.LoginResponse
import zisis.mobile.novibetdemo.providers.network.NetworkProvider
import zisis.mobile.novibetdemo.providers.sharedPreferences.SharedPreferencesProvider
import java.lang.ref.WeakReference
import java.util.concurrent.TimeUnit

class NovibetClient(appContext: Context) : NetworkProvider {

    private val novibetApi: NovibetApi
    private var applicationContext: WeakReference<Context>? = null
    private val sharedPreferencesProvider: SharedPreferencesProvider? = null

    init {
        applicationContext = WeakReference(appContext)
        novibetApi = createApi(appContext)
    }

    private fun createApi(appContext: Context): NovibetApi {
        return retrofit().create(NovibetApi::class.java)
    }

    private fun getOkHttpClient(): OkHttpClient {
        val logging = HttpLoggingInterceptor()
        logging.level = HttpLoggingInterceptor.Level.BODY

        val builder = OkHttpClient.Builder()
            .connectTimeout(60, TimeUnit.SECONDS)
            .readTimeout(60, TimeUnit.SECONDS)
            .addInterceptor(logging)
            .addInterceptor { chain ->
                val newUrl = chain.request().url
                    .newBuilder()
                    .build()

                val newRequest = chain.request()
                    .newBuilder()
                    .url(newUrl)
                    .addHeader("Authorization", "Bearer " + sharedPreferencesProvider?.getAccessToken())
                    .build()

                chain.proceed(newRequest)
            }
        return builder.build()
    }

    private fun retrofit(): Retrofit = Retrofit.Builder()
        .client(getOkHttpClient())
        .baseUrl(DefinitionsApi.DOMAIN)
        .addConverterFactory(GsonConverterFactory.create())
        .addCallAdapterFactory(CoroutineCallAdapterFactory()).build()

    override fun loginAsync(): Deferred<LoginResponse> {
        return novibetApi.userLoginAsync(DefinitionsApi.userName, DefinitionsApi.password)
    }

    override fun getHeadLinesAsync(): Deferred<List<HeadLineResponse>> {
        return novibetApi.getHeadLinesAsync()
    }

    override fun getGamesAsync(): Deferred<List<GameResponse>> {
        return novibetApi.getGamesAsync()
    }

    override fun updatedGamesAsync(): Deferred<List<GameResponse>> {
        return novibetApi.updateGamesAsync()
    }

    override fun updatedHeadLinesAsync(): Deferred<List<HeadLineResponse>> {
        return novibetApi.updateHeadLinesAsync()
    }


}