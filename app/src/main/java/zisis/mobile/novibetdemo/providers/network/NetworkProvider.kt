package zisis.mobile.novibetdemo.providers.network

import kotlinx.coroutines.Deferred
import zisis.mobile.novibetdemo.models.parsers.games.GameResponse
import zisis.mobile.novibetdemo.models.parsers.headLines.HeadLineResponse
import zisis.mobile.novibetdemo.models.parsers.login.LoginResponse

interface NetworkProvider {

    fun loginAsync(): Deferred<LoginResponse>
    fun getHeadLinesAsync(): Deferred<List<HeadLineResponse>>
    fun getGamesAsync(): Deferred<List<GameResponse>>
    fun updatedGamesAsync(): Deferred<List<GameResponse>>
    fun updatedHeadLinesAsync(): Deferred<List<HeadLineResponse>>
}