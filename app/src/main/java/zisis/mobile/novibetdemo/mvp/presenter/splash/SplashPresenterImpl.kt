package zisis.mobile.novibetdemo.mvp.presenter.splash

import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import timber.log.Timber
import zisis.mobile.novibetdemo.mvp.interactor.splash.SplashInteractor
import zisis.mobile.novibetdemo.mvp.presenter.base.BasePresenter
import zisis.mobile.novibetdemo.mvp.view.splash.SplashView

class SplashPresenterImpl(
    view: SplashView,
    interactor: SplashInteractor
) : BasePresenter<SplashView, SplashInteractor>(view, interactor), SplashPresenter {

    companion object {
        const val SPLASH_DELAY: Long = 3000
    }


    override fun init() {
        if (!isViewAttached()) {
            return
        }

        uiScope.launch {
            getView()?.showLoader()
            if (getInteractor()?.isUserLoggedIn() == false) {
                val response = withContext(bgDispatcher) {
                    getInteractor()?.login()
                }
                if (!isViewAttached()) {
                    return@launch
                }
                delay(SPLASH_DELAY)
                if (!isViewAttached()) {
                    return@launch
                }
                response?.data?.let { loginResponse ->
                    if (loginResponse.isSuccess()) {
                        getInteractor()?.setUserLoggedIn(true)
                        getInteractor()?.setAccessToken(loginResponse.getAccessToken() ?: "")
                        getView()?.goToHomeScreen()
                    } else {
                        getView()?.showLoginFail()
                    }
                } ?: response?.throwable?.let { throwable ->
                    Timber.d(throwable)
                } ?: run {
                    getView()?.showLoginFail()
                    getView()?.hideLoader()
                    return@launch
                }
            } else {
                delay(SPLASH_DELAY)
                getView()?.goToHomeScreen()
            }
        }
        getView()?.hideLoader()
    }
}