package zisis.mobile.novibetdemo.mvp.interactor.home

import zisis.mobile.novibetdemo.models.common.DataResult
import zisis.mobile.novibetdemo.models.parsers.games.GameResponse
import zisis.mobile.novibetdemo.models.parsers.headLines.HeadLineResponse
import zisis.mobile.novibetdemo.models.viewModels.game.GameModel
import zisis.mobile.novibetdemo.models.viewModels.headLine.HeadLineModel
import zisis.mobile.novibetdemo.mvp.interactor.base.MVPInteractor

interface HomeInteractor: MVPInteractor {

    suspend fun getHeaderLine(): DataResult<List<HeadLineModel>>
    suspend fun getGames(): DataResult<List<GameModel>>
}