package zisis.mobile.novibetdemo.mvp.view.splash

import zisis.mobile.novibetdemo.mvp.view.base.MVPView

interface SplashView: MVPView {
    fun goToHomeScreen()
    fun showLoginFail()
}