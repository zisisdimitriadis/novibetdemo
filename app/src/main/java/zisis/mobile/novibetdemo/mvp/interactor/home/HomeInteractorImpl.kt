package zisis.mobile.novibetdemo.mvp.interactor.home

import timber.log.Timber
import zisis.mobile.novibetdemo.models.common.DataResult
import zisis.mobile.novibetdemo.models.viewModels.game.GameModel
import zisis.mobile.novibetdemo.models.viewModels.game.toModelList
import zisis.mobile.novibetdemo.models.viewModels.headLine.HeadLineModel
import zisis.mobile.novibetdemo.models.viewModels.headLine.toModelList
import zisis.mobile.novibetdemo.mvp.interactor.base.BaseInteractor
import zisis.mobile.novibetdemo.providers.network.NetworkProvider

class HomeInteractorImpl(
    private val networkProvider: NetworkProvider
) : BaseInteractor(), HomeInteractor {


    override suspend fun getHeaderLine(): DataResult<List<HeadLineModel>> {
        return try {
            val response = networkProvider.getHeadLinesAsync().await()
            return DataResult(response.toModelList())
        } catch (t: Throwable) {
            Timber.d(t)
            DataResult(throwable = t)
        }
    }

    override suspend fun getGames(): DataResult<List<GameModel>> {
        return try {
            val response = networkProvider.getGamesAsync().await()
            return DataResult(response.toModelList())
        } catch (t: Throwable) {
            Timber.d(t)
            DataResult(throwable = t)
        }
    }
}