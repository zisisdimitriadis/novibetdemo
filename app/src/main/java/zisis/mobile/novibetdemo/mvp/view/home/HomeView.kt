package zisis.mobile.novibetdemo.mvp.view.home

import zisis.mobile.novibetdemo.models.viewModels.game.GameModel
import zisis.mobile.novibetdemo.models.viewModels.headLine.HeadLineModel
import zisis.mobile.novibetdemo.mvp.view.base.MVPView

interface HomeView: MVPView {

    fun showHomeView(headerList: ArrayList<HeadLineModel>, gamesList: ArrayList<GameModel>)
}