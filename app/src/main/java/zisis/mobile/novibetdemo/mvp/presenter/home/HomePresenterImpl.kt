package zisis.mobile.novibetdemo.mvp.presenter.home

import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import zisis.mobile.novibetdemo.common.extensions.toSeconds
import zisis.mobile.novibetdemo.models.viewModels.game.GameModel
import zisis.mobile.novibetdemo.models.viewModels.game.getModel
import zisis.mobile.novibetdemo.models.viewModels.headLine.HeadLineModel
import zisis.mobile.novibetdemo.mvp.interactor.home.HomeInteractor
import zisis.mobile.novibetdemo.mvp.presenter.base.BasePresenter
import zisis.mobile.novibetdemo.mvp.view.home.HomeView

class HomePresenterImpl(
    view: HomeView,
    interactor: HomeInteractor
) : BasePresenter<HomeView, HomeInteractor>(view, interactor), HomePresenter {

    private var headLineModelList: ArrayList<HeadLineModel> = arrayListOf()
    private var gamesModelList: ArrayList<GameModel> = arrayListOf()

    override fun init() {
        if (!isViewAttached()) {
            return
        }

        uiScope.launch {
            getView()?.showLoader()

            val headLineResponse = withContext(bgDispatcher) {
                getInteractor()?.getHeaderLine()
            }

            val gamesResponse = withContext(bgDispatcher) {
                getInteractor()?.getGames()
            }

            if (!isViewAttached()) {
                return@launch
            }

            headLineResponse?.data?.let { headList ->
                headLineModelList.addAll(headList)
            }

            gamesResponse?.data?.let { gamesList ->
                gamesModelList.addAll(gamesList)
            }

            if (headLineModelList.isEmpty() && gamesModelList.isEmpty()) {
                getView()?.showEmpty()
                getView()?.hideLoader()
                return@launch
            }

            getView()?.showHomeView(headLineModelList, gamesModelList)
            getView()?.hideLoader()
        }
    }

    override fun updateHeadLineList(updatedHeadLine: List<HeadLineModel>) {
        this.headLineModelList.clear()
        this.headLineModelList.addAll(updatedHeadLine)
        getView()?.showHomeView(this.headLineModelList, this.gamesModelList)
    }


    override fun updateGameList(updatedGames: List<GameModel>) {
        val newGameModelList: ArrayList<GameModel> = arrayListOf()
        updatedGames.forEach { gameModel ->
            val currentGameModel =
                this.gamesModelList.getModel(gameModel.competitor1, gameModel.competitor2)
            if (currentGameModel == null || gameModel.elapsed.toSeconds() - currentGameModel.elapsed.toSeconds() >= 20) {
                newGameModelList.add(gameModel)
            } else {
                newGameModelList.add(currentGameModel)
            }
        }
        this.gamesModelList.clear()
        this.gamesModelList.addAll(newGameModelList)
        getView()?.showHomeView(this.headLineModelList, this.gamesModelList)
    }

}