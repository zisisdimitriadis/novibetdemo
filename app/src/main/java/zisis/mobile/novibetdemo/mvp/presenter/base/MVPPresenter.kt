package zisis.mobile.novibetdemo.mvp.presenter.base

import zisis.mobile.novibetdemo.mvp.interactor.base.MVPInteractor
import zisis.mobile.novibetdemo.mvp.view.base.MVPView

interface MVPPresenter<out V: MVPView, out I: MVPInteractor> {
    fun detach()
    fun getView(): V?
    fun getInteractor(): I?
    fun isViewAttached() : Boolean
}