package zisis.mobile.novibetdemo.mvp.presenter.home

import zisis.mobile.novibetdemo.models.viewModels.game.GameModel
import zisis.mobile.novibetdemo.models.viewModels.headLine.HeadLineModel
import zisis.mobile.novibetdemo.mvp.interactor.home.HomeInteractor
import zisis.mobile.novibetdemo.mvp.presenter.base.MVPPresenter
import zisis.mobile.novibetdemo.mvp.view.home.HomeView

interface HomePresenter : MVPPresenter<HomeView, HomeInteractor> {

    fun init()
    fun updateHeadLineList(updatedHeadLine: List<HeadLineModel>)
    fun updateGameList(updatedGames: List<GameModel>)
}