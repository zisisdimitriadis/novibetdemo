package zisis.mobile.novibetdemo.mvp.interactor.base

interface MVPInteractor {
    fun detach()
}