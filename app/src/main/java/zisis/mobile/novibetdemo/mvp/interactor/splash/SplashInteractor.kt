package zisis.mobile.novibetdemo.mvp.interactor.splash

import zisis.mobile.novibetdemo.models.common.DataResult
import zisis.mobile.novibetdemo.models.parsers.login.LoginResponse
import zisis.mobile.novibetdemo.mvp.interactor.base.MVPInteractor

interface SplashInteractor: MVPInteractor {

    fun isUserLoggedIn(): Boolean
    fun setUserLoggedIn(value: Boolean)
    suspend fun login(): DataResult<LoginResponse>
    fun setAccessToken(token: String)
}