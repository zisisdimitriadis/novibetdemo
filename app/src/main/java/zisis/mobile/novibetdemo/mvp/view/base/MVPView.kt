package zisis.mobile.novibetdemo.mvp.view.base


interface MVPView {

    fun isAttached(): Boolean
    fun showError(error: String)
    fun showLoader()
    fun hideLoader()
    fun showEmpty()
    fun hideEmpty()
    fun showGenericError()
    fun showNoInternetError()
}