package zisis.mobile.novibetdemo.mvp.presenter.base

import kotlinx.coroutines.*
import zisis.mobile.novibetdemo.models.common.DataResult
import zisis.mobile.novibetdemo.mvp.interactor.base.MVPInteractor
import zisis.mobile.novibetdemo.mvp.view.base.MVPView
import java.lang.ref.WeakReference
import java.net.UnknownHostException

open class BasePresenter<out V : MVPView, out I : MVPInteractor>(view: V, interactor: I) :
    MVPPresenter<V, I> {

    private var viewRef: WeakReference<V>? = null
    private var interactor: I? = interactor

    protected var uiDispatcher: CoroutineDispatcher = Dispatchers.Main
    protected var bgDispatcher: CoroutineDispatcher = Dispatchers.IO

    protected var job = SupervisorJob()
    protected var uiScope = CoroutineScope(Dispatchers.Main + job)

    init {
        viewRef = WeakReference(view)
    }

    override fun getView(): V? {
        return viewRef?.get()
    }

    override fun getInteractor(): I? {
        return interactor
    }

    override fun isViewAttached(): Boolean {
        return viewRef != null && viewRef!!.get() != null && viewRef!!.get()!!.isAttached()
    }

    override fun detach() {
        uiScope.coroutineContext.cancelChildren()
        viewRef?.clear()
        interactor?.detach()
        interactor = null
    }

    //FIXME: for unit tests ONLY!!
    fun updateDispatchersForTests(
        uiDispatcher: CoroutineDispatcher,
        bgDispatcher: CoroutineDispatcher,
        uiScope: CoroutineScope
    ) {
        this.uiDispatcher = uiDispatcher
        this.bgDispatcher = bgDispatcher
        this.uiScope = uiScope
    }

    fun showError(dataResult: DataResult<*>?) {
        if (!isViewAttached()) {
            return
        }
        val errorMessage = dataResult?.throwable?.message
        if (errorMessage != null) {
            getView()?.showError(errorMessage)
        } else {
            getView()?.showGenericError()
        }
    }


    fun onErrorThrowable(throwable: Throwable, isShowEmptyView: Boolean = false) {
        getView()?.hideLoader()
        if (isNetworkError(throwable)) {
            getView()?.showNoInternetError()
        } else {
            getView()?.showGenericError()
        }
        if (isShowEmptyView) {
            getView()?.showEmpty()
        }
    }

    private fun isNetworkError(throwable: Throwable): Boolean {
        when (throwable) {
            is UnknownHostException -> {
                return true
            }
        }
        return false
    }

}