package zisis.mobile.novibetdemo.mvp.presenter.splash

import zisis.mobile.novibetdemo.mvp.interactor.splash.SplashInteractor
import zisis.mobile.novibetdemo.mvp.presenter.base.MVPPresenter
import zisis.mobile.novibetdemo.mvp.view.splash.SplashView

interface SplashPresenter : MVPPresenter<SplashView, SplashInteractor> {
    fun init()

}