package zisis.mobile.novibetdemo.mvp.interactor.splash

import timber.log.Timber
import zisis.mobile.novibetdemo.models.common.DataResult
import zisis.mobile.novibetdemo.models.parsers.login.LoginResponse
import zisis.mobile.novibetdemo.mvp.interactor.base.BaseInteractor
import zisis.mobile.novibetdemo.providers.network.NetworkProvider
import zisis.mobile.novibetdemo.providers.sharedPreferences.SharedPreferencesProvider

class SplashInteractorImpl(
    private val networkProvider: NetworkProvider,
    private val sharedPreferences: SharedPreferencesProvider
): BaseInteractor(), SplashInteractor {


    override fun isUserLoggedIn(): Boolean {
        return sharedPreferences.isUserLoggedIn()
    }

    override fun setUserLoggedIn(value: Boolean) {
        sharedPreferences.setUserLoggedIn(value)
    }

    override suspend fun login(): DataResult<LoginResponse> {
        return try {
            val response = networkProvider.loginAsync().await()
            DataResult(response)
        } catch (t: Throwable) {
            Timber.d(t)
            DataResult(throwable = t)
        }
    }

    override fun setAccessToken(token: String) {
        sharedPreferences.setAccessToken(token)
    }
}